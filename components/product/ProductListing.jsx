import Image from 'next/image'
import Link from 'next/link';
import React, { useContext } from 'react'
import { addToCart } from '../../store/Actions';
import { DataContext } from '../../store/GlobalStore';

const ProductListing = ({ data }) => {
    const { state, dispatch } = useContext(DataContext);
    const { cart } = state;
    return (
        <>
            <div class="flex flex-wrap items-center  overflow-x-auto overflow-y-hidden py-3 justify-center   bg-white text-gray-800">
                <a rel="noopener noreferrer" href="#" class="flex items-center flex-shrink-0 px-5 py-3 space-x-2text-gray-600">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="w-4 h-4">
                        <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path>
                    </svg>
                    <span>CLOTHES</span>
                </a>
                <a rel="noopener noreferrer" href="#" class="flex items-center flex-shrink-0 px-5 py-3 space-x-2 rounded-t-lg text-gray-900">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="w-4 h-4">
                        <path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z"></path>
                        <path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z"></path>
                    </svg>
                    <span>GADGETS</span>
                </a>
            </div>
            <section class="py-10 bg-gray-100">
                <div class="mx-auto grid max-full  grid-cols-1 gap-6 p-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                    {data?.map((product, _idx) => (
                        <>
                            <div class="w-full rounded overflow-hidden shadow-lg p-3">
                                <Image class="w-full" src={product.images[0].url} alt="Sunset in the mountains" width='500' height="500" style={{
                                    width: "500px",
                                    height: "200px",
                                    objectFit: "contain"
                                }} />
                                <div class="p-3">
                                    <div class="font-bold text-xl mb-2">{product.title}</div>
                                    <p class="text-gray-700 text-base">
                                        {product.description.substring(0, 60)} ...
                                    </p>
                                </div>
                                <div class="flex flex-wrap items-end justify-between p-3">
                                    <div class="flex w-full items-center justify-center rounded-lg bg-green-500 px-4 py-1.5 text-white duration-100 hover:bg-green-600 cursor-pointer">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M2.036 12.322a1.012 1.012 0 010-.639C3.423 7.51 7.36 4.5 12 4.5c4.638 0 8.573 3.007 9.963 7.178.07.207.07.431 0 .639C20.577 16.49 16.64 19.5 12 19.5c-4.638 0-8.573-3.007-9.963-7.178z" />
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                        </svg>

                                        <Link href={`product/${product._id}`} class="text-sm text-white ms-2"
                                        >View</Link>
                                    </div>
                                    <div class="flex w-full mt-2 items-center justify-center rounded-lg bg-blue-500 px-4 py-1.5 text-white duration-100 hover:bg-blue-600 cursor-pointer">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="h-5 w-5">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 00-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 00-16.536-1.84M7.5 14.25L5.106 5.272M6 20.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm12.75 0a.75.75 0 11-1.5 0 .75.75 0 011.5 0z" />
                                        </svg>
                                        <button class="text-sm ms-2" disabled={product.inStock === 0 ? true : false}
                                            onClick={() => dispatch(addToCart(product, cart))}>Add to cart</button>
                                    </div>
                                </div>
                                <div class="px-6 pt-4 pb-2">
                                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#bags</span>
                                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#fashion</span>
                                    <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                                </div>
                            </div>
                        </>
                    ))}
                </div>
            </section></>
    )
}

export default ProductListing