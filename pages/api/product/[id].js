import connectDB from "../../../utils/connectDB";
import Products from "../../../models/productModel";

connectDB();

export default async function (req, res) {
  switch (req.method) {
    case "GET":
      await getProduct(req, res);
      break;
  }
}

const getProduct = async (req, res) => {
  try {
    const { id } = req.query;
    const products = await Products.findById(id);
    if (!products)
      return res.status(400).json({ err: "This product  does not exist." });

    res.json({
      products,
    });
  } catch (error) {
    return res.status(500).json({ err: error.message });
  }
};
