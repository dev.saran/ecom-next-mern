import React, { useState } from "react";
import ProductItem from "../../components/product/ProductItem";
import { getData } from "../../utils/fetchData";

const ProductDetail = (props) => {
  const [productDetail, setProductDetail] = useState(props.product);
  return  <ProductItem product={productDetail}/>
};

export default ProductDetail;

export async function getServerSideProps({ params: { id } }) {
  const res = await getData(`product/${id}`);
  //server side rendering
  console.log(res.products);
  return {
    props: {
      product: res.products,
    },
  };
}
